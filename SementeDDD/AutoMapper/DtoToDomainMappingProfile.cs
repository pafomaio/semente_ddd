﻿using AutoMapper;
using SementeDDD.DTO;
using SementeDDD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SementeDDD.AutoMapper
{
    public class DtoToDomainMappingProfile : Profile
    {
        public DtoToDomainMappingProfile()
        {
            CreateMap<FarmacoDto, Farmaco>();
        }

    }
}
