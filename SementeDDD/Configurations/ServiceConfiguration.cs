﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using SementeDDD.Services;
using SementeDDD.Services.Default;

namespace SementeDDD.Configurations
{
    public class ServiceConfiguration
    {
        public static void configure(IConfiguration configuration, IServiceCollection services)
        {
            useDefault(configuration, services);
        }

        private static void useDefault(IConfiguration configuration, IServiceCollection services)
        {
            services.AddTransient<IFarmacoService, FarmacoService>();
        }
    }
}
