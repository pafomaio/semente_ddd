﻿using SementeDDD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SementeDDD.Repository
{
    public interface IRepository<T> where T : BaseEntity
    {
        T GetById(int id);

        List<T> List();

        List<T> List(ISpecification<T> spec);

        IQueryable<T> GetQueryable();

        IQueryable<T> GetQueryable(ISpecification<T> spec);

        T Add(T entity);

        void Update(T entity);

        void Delete(T entity);

    }
}
