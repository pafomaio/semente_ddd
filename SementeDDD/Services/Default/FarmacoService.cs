﻿using SementeDDD.DTO;
using SementeDDD.Entities;
using SementeDDD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace SementeDDD.Services.Default
{
    public class FarmacoService : IFarmacoService
    {
        private readonly IMapper _mapper;
        private readonly IFarmacoRepository _farmacoRepository;

        public FarmacoService(IMapper mapper, IFarmacoRepository farmacoRepository)
        {
            _mapper = mapper;
            _farmacoRepository = farmacoRepository;
        }

        private Farmaco GetFarmacoById(int id)
        {
            return _farmacoRepository.GetById(id);
        }

        public IEnumerable<FarmacoDto> GetAll()
        {
            return _farmacoRepository.GetQueryable().ProjectTo<FarmacoDto>();
        }

        public FarmacoDto GetById(int id)
        {
            return _mapper.Map<FarmacoDto>(GetFarmacoById(id));
        }

        public FarmacoDto Register(FarmacoDto dto)
        {
            if (dto == null)
                return null;
            
            Farmaco farmaco = _mapper.Map<Farmaco>(dto);
            farmaco = _farmacoRepository.Add(farmaco);
            return _mapper.Map<FarmacoDto>(farmaco);
            
        }

        public void Remove(int id)
        {
            Farmaco farmaco = GetFarmacoById(id);
            if (farmaco != null)
                _farmacoRepository.Delete(farmaco);
        }

        public void Update(FarmacoDto dto)
        {
            if (dto == null)
                return;
            Farmaco farmaco = GetFarmacoById(dto.Id);
            if (farmaco != null)
            {
                // Copy updatable fields only
                farmaco.Name = dto.Name;
                farmaco.ShortDescription = dto.ShortDescription;
                farmaco.DetailedDescription = dto.DetailedDescription;
                _farmacoRepository.Update(farmaco);
            }
        }

        public bool Exists(int id)
        {
            Farmaco farmaco = GetFarmacoById(id);

            return farmaco != null ? true : false;
        }

    }
}
