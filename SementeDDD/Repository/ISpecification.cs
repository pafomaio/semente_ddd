﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SementeDDD.Repository
{
    public interface ISpecification<T>
    {
        Expression<Func<T, bool>> Criteria { get; }

        Expression<Func<T, object>> Include { get; }
    }
}
