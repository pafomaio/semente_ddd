﻿using SementeDDD.DTO;
using SementeDDD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SementeDDD.Services
{
    public interface IFarmacoService:IService<Farmaco,FarmacoDto>
    {
    }
}
