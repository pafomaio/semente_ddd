﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SementeDDD.Entities
{
    public class Farmaco: BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; } = System.DateTime.Now;

        public string ShortDescription { get; set; }

        public string DetailedDescription { get; set; }

		public Farmaco()
		{
			
		}

        public Farmaco(string name, string shortDescription, string detailedDescription)
        {
            this.Name = name;
            this.ShortDescription = shortDescription;
            this.DetailedDescription = detailedDescription;
        }
        

    }
}
