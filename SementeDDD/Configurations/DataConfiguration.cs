﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using SementeDDD.Data.EF;
using SementeDDD.Repository;


namespace SementeDDD.Configurations
{
    public enum DataProviderEnum
    {
        InMemory = 0,
        SQLServer = 1

    }
    public class DataConfiguration
    {
        public static void configure(DataProviderEnum provider, IConfiguration configuration, IServiceCollection services)
        {
            switch(provider)
            {
                case DataProviderEnum.InMemory:
                    addInMemory(configuration, services);
                    break;
                case DataProviderEnum.SQLServer:
                    addSqlServer(configuration, services);
                    break;
            }
            useEntityFramework(configuration, services);
        }

        private static void addInMemory(IConfiguration configuration, IServiceCollection services)
        {
            services.AddDbContext<EfDbContext>(options =>
                   options.UseInMemoryDatabase("SementeDDD_DB"));

        }

        private static void addSqlServer(IConfiguration configuration, IServiceCollection services)
        {
            services.AddDbContext<EfDbContext>(options =>
                   options.UseSqlServer(configuration.GetConnectionString("EfDbContext")));
        }

        private static void useEntityFramework(IConfiguration configuration, IServiceCollection services)
        {
            // AddScoped(): These services are created once per request.
            // AddTransient(): These services are created each time they are requested.
            // AddSingleton(): These services are created first time they are requested and stay the same for subsequence requests.

            services.AddTransient<IFarmacoRepository, EfFarmacoRepository>();
        }
    }
}
