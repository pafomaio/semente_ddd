﻿using SementeDDD.Entities;
using SementeDDD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SementeDDD.Data.EF
{
    public class EfFarmacoRepository: EfRepository<Farmaco>, IFarmacoRepository
    {
        public EfFarmacoRepository(EfDbContext dbContext) : base(dbContext)
        {

        }
    }
}
