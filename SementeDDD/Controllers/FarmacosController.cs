﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SementeDDD.Data;
using SementeDDD.Entities;
using SementeDDD.Services;
using SementeDDD.DTO;

namespace SementeDDD.Controllers
{
    [Produces("application/json")]
    [Route("api/Farmacos")]
    public class FarmacosController : Controller
    {
        private readonly IFarmacoService _farmacoService;

        public FarmacosController(IFarmacoService farmacoService)
        {
            _farmacoService = farmacoService;
        }

        // GET: api/Farmacos
        [HttpGet]
        public IEnumerable<FarmacoDto> GetFarmaco()
        {
            return _farmacoService.GetAll();
        }

        // GET: api/Farmacos/5
        [HttpGet("{id}")]
        public IActionResult GetFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = _farmacoService.GetById(id);

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }

        // PUT: api/Farmacos/5
        [HttpPut("{id}")]
        public IActionResult PutFarmaco([FromRoute] int id, [FromBody] FarmacoDto farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.Id)
            {
                return BadRequest();
            }

            if (!FarmacoExists(id))
            {
                return NotFound();
            }
            else
            {
                _farmacoService.Update(farmaco);
            }

            return NoContent();
        }

        // POST: api/Farmacos
        [HttpPost]
        public IActionResult PostFarmaco([FromBody] FarmacoDto farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            farmaco = _farmacoService.Register(farmaco);

            return CreatedAtAction("GetFarmaco", new { id = farmaco.Id }, farmaco);
        }

        // DELETE: api/Farmacos/5
        [HttpDelete("{id}")]
        public IActionResult DeleteFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!FarmacoExists(id))
            {
                return NotFound();
            }
            else
            {
                _farmacoService.Remove(id);
            }

            return Ok();
        }

        private bool FarmacoExists(int id)
        {
            return _farmacoService.Exists(id);
        }
    }
}