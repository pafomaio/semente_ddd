using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using Newtonsoft.Json;
using SementeDDD.DTO;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace SementeDDDTesting
{
    public class FarmacoWebTesting
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        private readonly String _urlBase = "/api/farmacos/";
        private readonly int _notFoundId = -1;
        public FarmacoWebTesting()
        {
            // Arrange
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<SementeDDD.Startup>()
                .UseConfiguration(getConfiguration()));
            _client = _server.CreateClient();
        }

        private IConfiguration getConfiguration()
        {
            // Get configuration.
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Path.GetFullPath(@"../../../../SementeDDD/"))
                .AddJsonFile("appsettings.json", optional: false)
                //.AddUserSecrets<Startup>()
                .Build();

            return configuration;
        }
        private HttpContent createHttpContentForObjectAsJson(Object obj)
        {
            HttpContent content = new StringContent(JsonConvert.SerializeObject(obj));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return content;
        }


		[Fact]
		public async Task CheckListOfFarmacos()
		{
            int count = await CheckGetAllFarmacos();

            // Assert
            Assert.IsType<int>(count);
		}

		[Fact]
		public async Task CheckGetFarmacoNotFound()
		{
			// Act
			var response = await _client.GetAsync(_urlBase + _notFoundId);

            // Assert
            Assert.True(response.StatusCode.Equals(HttpStatusCode.NotFound));
		}

        [Fact]
        public async Task CheckPutFarmacoNotFound()
        {
            // Arrange
            FarmacoDto farmaco = new FarmacoDto();
            farmaco.Id = _notFoundId;
            farmaco.Name = "nome alterado";
            farmaco.ShortDescription = "short alterado";
            farmaco.DetailedDescription = "detailed alterado";

            HttpContent requestContent = createHttpContentForObjectAsJson(farmaco);

            // Act Put
            var response = await _client.PostAsync(_urlBase + farmaco.Id, requestContent);

            // Assert Put
            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode.Equals(HttpStatusCode.NotFound));
        }

        [Fact]
        public async Task<bool> CheckDeleteFarmacoNotFound()
        {
            // Act Delete
            var response = await _client.DeleteAsync(_urlBase + _notFoundId);

            // Assert Delete
            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode.Equals(HttpStatusCode.NotFound));

            return true;
        }

        [Fact]
		public async Task CheckCrudFarmaco()
		{
            int countInitial = await CheckGetAllFarmacos();
            int postResult = await CheckPostFarmaco();
            int countAfterPost = await CheckGetAllFarmacos();
            bool getResult = await CheckGetFarmaco(postResult);
            bool putResult = await CheckPutFarmaco(postResult);
            bool deleteResult = await CheckDeleteFarmaco(postResult);
            int countAfterDelete = await CheckGetAllFarmacos();

            Assert.NotNull(postResult);
            Assert.True(countAfterPost > countInitial);
            Assert.True(getResult);
            Assert.True(putResult);
            Assert.True(deleteResult);
            Assert.True(countAfterDelete == countInitial);
        }

        private async Task<int> CheckPostFarmaco()
        {
            // Arrange
            FarmacoDto farmaco = new FarmacoDto();
            farmaco.Name = "nome";
            farmaco.ShortDescription = "short";
            farmaco.DetailedDescription = "detailed";

            HttpContent requestContent = createHttpContentForObjectAsJson(farmaco);

            // Act Post
            var response = await _client.PostAsync(_urlBase, requestContent);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<FarmacoDto>(content);

            // Assert Post
            Assert.True(response.IsSuccessStatusCode);
            Assert.NotNull(result);
            Assert.IsType<FarmacoDto>(result);
            Assert.Equal(farmaco.Name, result.Name);
            Assert.Equal(farmaco.ShortDescription, result.ShortDescription);
            Assert.Equal(farmaco.DetailedDescription, result.DetailedDescription);
            Assert.InRange<int>(result.Id, 0, int.MaxValue);

            return result.Id;
        }

        private async Task<bool> CheckGetFarmaco(int id)
        {
            // Act Get/Id
            var response = await _client.GetAsync(_urlBase + id);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<FarmacoDto>(content);

            // Assert Get/Id
            Assert.True(response.IsSuccessStatusCode);
            Assert.NotNull(result);
            Assert.IsType<FarmacoDto>(result);
            Assert.Equal(id, result.Id);

            return true;
        }

        private async Task<bool> CheckPutFarmaco(int id)
        {
            // Arrange
            FarmacoDto farmaco = new FarmacoDto();
            farmaco.Id = id;
            farmaco.Name = "nome alterado";
            farmaco.ShortDescription = "short alterado";
            farmaco.DetailedDescription = "detailed alterado";

            HttpContent requestContent = createHttpContentForObjectAsJson(farmaco);

            // Act Put
            var response = await _client.PutAsync(_urlBase + id, requestContent);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<FarmacoDto>(content);

            // Assert Put
            Assert.True(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode.Equals(HttpStatusCode.NoContent));

            return true;
        }

        private async Task<bool> CheckDeleteFarmaco(int id)
        {
            // Act Delete
            var response = await _client.DeleteAsync(_urlBase + id);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<FarmacoDto>(content);

            // Assert Delete
            Assert.True(response.IsSuccessStatusCode);
            
            return true;
        }

        private async Task<int> CheckGetAllFarmacos()
        {
            // Act
            var response = await _client.GetAsync(_urlBase);
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<List<Object>>(responseString);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<Object>>(result);

            return result.Count;
        }
    }
}
