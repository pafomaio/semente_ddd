﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SementeDDD.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
