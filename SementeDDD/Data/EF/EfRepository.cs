﻿using SementeDDD.Entities;
using SementeDDD.Repository;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace SementeDDD.Data.EF
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {

        protected readonly EfDbContext _dbContext;

        public EfRepository(EfDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual T GetById(int id)
        {
            return _dbContext.Set<T>()
                .SingleOrDefault(e => e.Id == id);
        }


        public IQueryable<T> GetQueryable()
        {
            return _dbContext.Set<T>();
        }

        public IQueryable<T> GetQueryable(ISpecification<T> spec)
        {
            return _dbContext.Set<T>()
                .Include(spec.Include)
                .Where(spec.Criteria);
        }

        public List<T> List()
        {
            return this.GetQueryable().ToList();
        }

        public List<T> List(ISpecification<T> spec)
        {
            return this.GetQueryable(spec).ToList();
        }

        public T Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            _dbContext.SaveChanges();

            return entity;
        }

        public void Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            _dbContext.SaveChanges();
        }

        public void Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

    }
}
