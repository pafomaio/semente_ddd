﻿using SementeDDD.DTO;
using SementeDDD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SementeDDD.Services
{
    public interface IService<Entity,DTO> where Entity : BaseEntity where DTO: BaseDto
    {
        DTO Register(DTO dto);
        IEnumerable<DTO> GetAll();
        DTO GetById(int id);
        void Update(DTO dto);
        void Remove(int id);
        bool Exists(int id);
    }
}
