﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SementeDDD.Entities;

namespace SementeDDD.Data.EF
{
    public class EfDbContext : DbContext
    {
        public EfDbContext (DbContextOptions<EfDbContext> options)
            : base(options)
        {
        }

        public DbSet<SementeDDD.Entities.Farmaco> Farmaco { get; set; }
    }
}
